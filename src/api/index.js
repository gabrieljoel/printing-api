// api/index.js

import Vue from 'vue'

import axios from 'axios'
import store from '@/store'

let $axios = axios.create({
  baseURL: location.origin + '/api/',
  timeout: 20000
})

$axios.interceptors.request.use(function (config) {
  var token = ''

  if (config.hasOwnProperty('refresh') && config.refresh) {
    token = store.getters.getRefresh
  } else {
    token = store.getters.getJWT
  }

  if (token) {
    config.headers.Authorization = `Bearer ${token}`
  }

  return config
})

function errorResponseHandler (error) {
  const originalRequest = error.config

  // check for errorHandle config
  if (originalRequest.hasOwnProperty('errorHandle') && originalRequest.errorHandle === false) {
    return Promise.reject(error)
  }

  if (error.response.status === 401 && !originalRequest._retry) {
    originalRequest._retry = true

    return $axios.post('refresh/', {}, { refresh: true })
      .then(res => {
        if (res.status === 201) {
          store.commit('setJwtToken', { jwt: res.data.access_token })

          return $axios(originalRequest)
        }
      })
  }

  // if has response show the error
  if (error.response) {
    console.log(error.response)
    Vue.toasted.error(error.response.data.message).goAway(3500)
  }
}

$axios.interceptors.response.use(
  response => response,
  errorResponseHandler
)

export function authenticate (userData) {
  return $axios.post(`login/`, userData)
}

export function register (userData) {
  return $axios.post(`register/`, userData)
}

export function fetchProducts (storeId, productIds) {
  return $axios.post(`store/${storeId}/search/products/`, {
    productIds: productIds
  })
    .then(response => response.data)
}

export function fetchOrders (storeId, status, page) {
  return $axios.get(`store/${storeId}/orders/`, {
    params: {
      status: status,
      page: page
    }
  })
    .then(response => response.data)
}

export function fetchProduct (storeId, productId) {
  return $axios.get(`store/${storeId}/product/${productId}`)
    .then(response => response.data)
}

export function updateProduct (storeId, productId, data) {
  return $axios.put(`store/${storeId}/orders/${productId}`, {
    data: data
  })
    .then(response => response.data)
}

export function fetchShippingLabel (storeId, orderData) {
  return $axios.post(`store/${storeId}/shipping/label/`, orderData)
    .then(response => response.data)
}

export function fetchStores () {
  return $axios.get(`stores/`)
    .then(response => response.data)
}
