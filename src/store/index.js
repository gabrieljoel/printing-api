// store/index.js

import Vue from 'vue'
import Vuex from 'vuex'

// imports of AJAX functions will go here
import { authenticate, register, fetchStores } from '@/api'
import { isValidJwt } from '@/utils'

Vue.use(Vuex)

const state = {
  // single source of data
  stores: [],
  currentStore: 1,
  user: {},
  jwt: '',
  refresh: ''
}

const actions = {
  // asynchronous operations
  login (context, userData) {
    context.commit('setUserData', { userData })
    return authenticate(userData)
      .then(
        response => {
          context.commit('setJwtToken', { jwt: response.data.access_token })
          context.commit('setJwtRefreshToken', { refresh: response.data.refresh_token })
        })
  },
  logout (context) {
    context.commit('logout')
  },
  register (context, userData) {
    context.commit('setUserData', { userData })
    return register(userData)
      .then(context.dispatch('login', userData))
  },
  loadTokens ({ commit }) {
    let token = localStorage.token
    let refresh = localStorage.refresh

    if (token) {
      commit('setJwtToken', { jwt: token })
    }

    if (refresh) {
      commit('setJwtRefreshToken', { refresh: refresh })
    }
  },
  fetchStores ({ commit }) {
    return fetchStores()
      .then(
        response => {
          commit('setStores', { stores: response })
        }
      )
  },
  setCurrentStore (context, store) {
    context.commit('setCurrentStore', { store: store })
  }
}

const mutations = {
  // isolated data mutations
  setUserData (state, payload) {
    console.log('setUserData payload = ', payload)
    state.userData = payload.userData
  },
  setJwtToken (state, payload) {
    console.log('setJwtToken payload = ', payload)
    localStorage.token = payload.jwt
    state.jwt = payload.jwt
  },
  setJwtRefreshToken (state, payload) {
    console.log('setJwtRefreshToken payload = ', payload)
    localStorage.refresh = payload.refresh
    state.refresh = payload.refresh
  },
  logout (state) {
    state.userData = null
    localStorage.token = null
    state.jwt = ''
    state.refresh = ''
    localStorage.refresh = null
  },
  setStores (state, payload) {
    state.stores = payload.stores
  },
  setCurrentStore (state, payload) {
    state.currentStore = payload.store
  }
}

const getters = {
  // reusable data accessors
  isAuthenticated (state) {
    return isValidJwt(state.jwt)
  },
  getJWT (state) {
    return state.jwt
  },
  getRefresh (state) {
    return state.refresh
  },
  getStores (state) {
    return state.stores
  },
  getCurrentStore (state) {
    return state.currentStore
  }
}

const store = new Vuex.Store({
  state,
  actions,
  mutations,
  getters
})

export default store
