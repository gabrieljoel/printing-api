import Vue from 'vue'
import VueContentPlaceholders from 'vue-content-placeholders'
import VModal from 'vue-js-modal'
import VToasted from 'vue-toasted'

import App from './App.vue'
import router from './router'
import store from './store'

import './filters'

import * as Sentry from '@sentry/browser'
import * as Integrations from '@sentry/integrations'

Vue.config.productionTip = false

Vue.use(VueContentPlaceholders)
Vue.use(VModal, { dialog: true })
Vue.use(VToasted)

Sentry.init({
  dsn: 'https://7fc56a442a824183bb77b966d37bbde1:2fd65418996e4c78baf3d7294206c5e7@sentry.io/1732434',
  integrations: [new Integrations.Vue({ Vue, attachProps: true, logErrors: true })]
})

store.dispatch('loadTokens').then(() => {
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
})
