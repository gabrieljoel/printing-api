import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home.vue'
import Orders from './views/Orders.vue'
import Login from './views/Login.vue'
import NotFound from './views/NotFound.vue'
import LoggedOut from './views/LoggedOut.vue'

import store from '@/store'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: '/app/',
  routes: [
    {
      path: '/home',
      name: 'home',
      alias: '/',
      component: Home,
      meta: { requiresAuth: false }
    },
    {
      path: '/orders',
      name: 'orders',
      component: Orders,
      meta: { requiresAuth: true }
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: { requiresAuth: false }
    },
    {
      path: '/logout',
      name: 'logout',
      component: LoggedOut,
      meta: { requiresAuth: true }
    },
    { path: '*',
      name: 'not-found',
      component: NotFound,
      meta: { requiresAuth: false }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (
    // The route requires auth but the user is not logged in
    to.matched.some(record => record.meta.requiresAuth) &&
    !store.getters.isAuthenticated
  ) {
    // take user to login
    next('/login')
  } else {
    next()
  }
})

export default router
