# -*- coding: utf-8 -*-
""" Client App """

import os, re
from flask import Blueprint, current_app, send_file

client_bp = Blueprint(
    "client_app",
    __name__,
    url_prefix="",
    static_url_path="",
    static_folder="./dist/static/",
    template_folder="./dist/",
)


@client_bp.route("/")
def index_client():
    dist_dir = current_app.config["DIST_DIR"]
    entry = os.path.join(dist_dir, "index.html")
    return send_file(entry)


@client_bp.route("/app/<path:path>")
def frontend(path):
    filetypes = re.compile(r".*\.(?:css|ico|svg|png|js|map|json)$", re.IGNORECASE)
    dist_dir = current_app.config["DIST_DIR"]

    if re.match(filetypes, path):
        entry = os.path.join(dist_dir, path)
        return send_file(entry)

    entry = os.path.join(dist_dir, "index.html")
    return send_file(entry)
