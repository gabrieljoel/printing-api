import click
import requests
import os
import time
import sys
import uuid
from functools import wraps, lru_cache

from flask import Flask

from miseru.api.resources import get_wcapi, get_product_rows
from miseru.models import WooStore

app = Flask(__name__)

ID = "id"
NAME = "nombre"
DESCRIPTION = "descripción"
SIZE_VARIATIONS = "variaciones-tamaño"
PRICE_VARIATIONS = "variaciones-precios"
DEFAULT_SIZE = "tamaño-defecto"
COLOR_VARIATIONS = "variaciones-color"
DEFAULT_COLOR = "color-defecto"
BRAND = "marca"
CATEGORY = "categoria"
PRICE = "precio"
MAIN_IMAGE = "imagen-principal"
ADDITIONAL_IMAGES = "imagenes-adicionales"
IMAGE_FOR_PRINT = "imagen-para-impresión"

SIMPLE_PRODUCT = "simple"
VARIABLE_PRODUCT = "variable"

SIZE_ATTRIBUTE = "Size"
COLOR_ATTRIBUTE = "Color"

def retry(exceptions, tries=5, delay=10, backoff=2, logger=None):
    """
    Retry calling the decorated function using an exponential backoff.

    Args:
        exceptions: The exception to check. may be a tuple of
            exceptions to check.
        tries: Number of times to try (not retry) before giving up.
        delay: Initial delay between retries in seconds.
        backoff: Backoff multiplier (e.g. value of 2 will double the delay
            each retry).
        logger: Logger to use. If None, print.
    """
    def deco_retry(f):

        @wraps(f)
        def f_retry(*args, **kwargs):
            mtries, mdelay = tries, delay
            while mtries > 1:
                try:
                    return f(*args, **kwargs)
                except exceptions as e:
                    msg = '{}, Retrying in {} seconds...'.format(e, mdelay)
                    if logger:
                        logger.warning(msg)
                    else:
                        print(msg)
                    time.sleep(mdelay)
                    mtries -= 1
                    mdelay *= backoff
            return f(*args, **kwargs)

        return f_retry  # true decorator

    return deco_retry

@app.cli.command("import-products")
@click.option("--store-id", default=2, help="Store id in stores table")
@click.option("--sheet-name", default="Productos Tienda Mindcrafted", help="Google Spreadsheet name")
@click.option("--auth-name", default="admin", help="Auth name for Application Passwords extension")
@click.option("--auth-pass", default="ZGsviK1V1YNSSoXIsbzDJrk3", help="Auth password for Application Passwords extension")
def import_products(store_id, sheet_name, auth_name, auth_pass):
    """Import products from Google Sheets into WooCommerce
    """
    wcapi = get_wcapi(store_id)
    store_products = get_product_rows(sheet_name)
    products = []

    for p in store_products:
        id = p[ID]

        if id:
            print('Product already exists')
            continue

        images = get_images(p, store_id, auth_name, auth_pass)
        categories = get_categories(p, wcapi)

        sizes = list(filter(None, p[SIZE_VARIATIONS].split(",")))
        colors = list(filter(None, p[COLOR_VARIATIONS].split(",")))

        product_type = ""
        if sizes or colors:
            product_type = VARIABLE_PRODUCT
        else:
            product_type = SIMPLE_PRODUCT

        data = get_product_data(p, product_type, categories, images, wcapi)
        product = post_product(wcapi, data)
        products.append(product)

        if product_type == VARIABLE_PRODUCT:
            for size in sizes:
                # TODO: Take care of variation prices
                attribute_id = get_attribute_id(product["attributes"], SIZE_ATTRIBUTE)
                create_variation(product["id"], attribute_id, size, data["regular_price"], wcapi)
            for color in colors:
                attribute_id = get_attribute_id(product["attributes"], COLOR_ATTRIBUTE)
                create_variation(product["id"], attribute_id, color, data["regular_price"], wcapi)

    return products

@retry(Exception)
def post_product(wcapi, data):
    product = wcapi.post("products", data).json()

    return product

def get_images(product, store_id, auth_name, auth_pass):
    main_image = product[MAIN_IMAGE]
    images = product[ADDITIONAL_IMAGES].split(",")
    img_urls = [main_image] + images
    img_urls = filter(None, img_urls)
    uploaded_urls = [rest_img_upload(store_id, url, auth_name, auth_pass) for url in img_urls]
    images = [{"src": url} for url in uploaded_urls]

    return images

def get_categories(product, wcapi):
    category_names = [product[CATEGORY], product[BRAND]]
    categories = [get_category_id(wcapi, name) for name in category_names]

    return categories

def get_product_data(product, product_type, categories, images, wcapi):
    data = generate_basic_product_data(product, product_type, categories, images)

    if product_type == VARIABLE_PRODUCT:
        sizes = product[SIZE_VARIATIONS].split(",")
        colors = product[COLOR_VARIATIONS].split(",")

        size_attribute = get_product_attribute(wcapi, SIZE_ATTRIBUTE, sizes)
        color_attribute = get_product_attribute(wcapi, COLOR_ATTRIBUTE, colors)

        all_attributes = [(size_attribute, sizes), (color_attribute, colors)]
        data["attributes"] = [generate_product_attribute(a, o) for a, o in all_attributes]

        # TODO: Deal with case of no size, but color and vice-versa

        default_size = get_default_attribute(size_attribute, product[DEFAULT_SIZE])
        default_color = get_default_attribute(color_attribute, product[DEFAULT_COLOR])

        data["default_attributes"] = [default_size, default_color]

    return data

def generate_basic_product_data(product, product_type, categories, images):
     return {
        "name": product[NAME],
        "type": product_type,
        "regular_price": str(product[PRICE]),
        "description": product[DESCRIPTION],
        "categories": categories,
        "images": images
     }

@retry(Exception)
def create_variation(product_id, attribute_id, option, price, wcapi):
    data = {
        "regular_price": price,
        "attributes": [
            {
                "id": attribute_id,
                "option": option
            }
        ]
    }

    wcapi.post(f"products/{product_id}/variations", data).json()

def get_default_attribute(attribute, default_attribute_name):
    return {
        "id": attribute["id"],
        "option": default_attribute_name
    }

def get_attribute_id(attributes, attribute_name):
    matching = [a for a in attributes if a["name"] == attribute_name]
    attribute = matching[0]

    return attribute["id"]

@retry(Exception)
def get_product_attribute(wcapi, attribute_name, attribute_terms):
    attributes = wcapi.get("products/attributes").json()
    matching = [a for a in attributes if a["name"] == attribute_name]

    # TODO: Add extra attributes as they appear

    if len(matching) > 0:
        return matching[0]
    else:
        create_product_attribute(wcapi, attribute_name, attribute_terms)

@retry(Exception)
def create_product_attribute(wcapi, attribute_name, attribute_terms):
    data = { "name": attribute_name }
    attribute = wcapi.post("products/attributes", data).json()
    attribute_id = attribute["id"]

    for term in attribute_terms:
        data = { "name": term }
        result = wcapi.post(f"products/attributes/{attribute_id}/terms", data).json()

    return attribute

def generate_product_attribute(attribute, options):
    return {
        "id": attribute["id"],
        "position": 0,
        "variation": True,
        "visible": True,
        "options": options
    }

@lru_cache(maxsize=128)
@retry(Exception)
def get_category_id(wcapi, category_name):
    slug = category_name.lower()
    results = wcapi.get(f"products/categories?slug='{slug}'").json()

    category_id = 0

    if len(results) == 0:
        data = { "name": category_name }
        category = wcapi.post("products/categories", data).json()
        category_id = category["id"]

    else:
        category_id = results[0]["id"]

    return { "id" : category_id }

@retry(Exception)
def rest_img_upload(store_id, img_url, auth_name, auth_pass):
    file_id = img_url.split("id=")[1]
    filename = str(uuid.uuid4()) + ".jpg"
    download_file_from_google_drive(file_id, f"./{filename}")

    data = open(filename, 'rb').read()

    store = WooStore.query.get(store_id)
    upload_url = store.woo_url + "wp-json/wp/v2/media"

    res = requests.post(url=upload_url,
                        data=data,
                        headers={
                            'Content-Type': 'image/jpg',
                            'Content-Disposition' : 'attachment; filename=%s'% filename
                        },
                        auth=(auth_name, auth_pass))

    if res.status_code == 401:
        print("Auth pass or name is incorrect.")
        sys.exit()

    os.remove(filename)

    new_dict = res.json()
    link = new_dict.get("guid").get("rendered")

    return link

def download_file_from_google_drive(id, destination):
    URL = "https://docs.google.com/uc?export=download"

    session = requests.Session()

    response = session.get(URL, params = { 'id' : id }, stream = True)
    token = get_confirm_token(response)

    if token:
        params = { 'id' : id, 'confirm' : token }
        response = session.get(URL, params = params, stream = True)

    save_response_content(response, destination)

def get_confirm_token(response):
    for key, value in response.cookies.items():
        if key.startswith('download_warning'):
            return value

    return None

def save_response_content(response, destination):
    CHUNK_SIZE = 32768

    with open(destination, "wb") as f:
        for chunk in response.iter_content(CHUNK_SIZE):
            if chunk: # filter out keep-alive new chunks
                f.write(chunk)
