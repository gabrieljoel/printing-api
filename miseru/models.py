"""
models.py
- Data classes for the surveyapi application
"""
from dataclasses import dataclass

from werkzeug.security import generate_password_hash, check_password_hash

from miseru.extensions import db


class User(db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)
    authorized = db.Column(db.Boolean, default=False)

    def __init__(self, email, password):
        self.email = email
        self.password = generate_password_hash(password, method="sha256")

    @classmethod
    def authenticate(cls, email, password):
        if not email or not password:
            return None

        user = cls.query.filter_by(email=email).first()
        if not user or not check_password_hash(user.password, password):
            return None

        return user

    def to_dict(self):
        return dict(id=self.id, email=self.email)

@dataclass
class WooStore(db.Model):
    id: int
    store_name: str
    woo_consumer_key: str
    woo_consumer_secret: str
    woo_url: str
    store_url: str
    easypost_id: int
    stripe_id: int

    __tablename__ = "stores"

    id = db.Column(db.Integer, primary_key=True)
    store_name = db.Column(db.String(100), nullable=False)
    woo_consumer_key = db.Column(db.Text, nullable=False)
    woo_consumer_secret = db.Column(db.Text, nullable=False)
    woo_url = db.Column(db.Text, unique=False, nullable=False)
    store_url = db.Column(db.Text, unique=True, nullable=False)
    easypost_id = db.Column(
        db.ForeignKey("easypost.id", onupdate="CASCADE", ondelete="CASCADE")
    )
    stripe_id = db.Column(
        db.ForeignKey("stripe.id", onupdate="CASCADE", ondelete="CASCADE")
    )
    easypost = db.relationship("EasyPost", foreign_keys=[easypost_id])
    stripe = db.relationship("Stripe", foreign_keys=[stripe_id])

    def to_dict(self):
        return dict(id=self.id,
                    store_name=self.store_name,
                    woo_consumer_key=self.woo_consumer_key,
                    woo_consumer_secret=self.woo_consumer_secret,
                    woo_url=self.woo_url,
                    store_url=self.store_url,
                    easypost_id=self.easypost_id,
                    stripe_id=self.stripe_id)



@dataclass
class EasyPost(db.Model):
    id: int
    key_name: str
    api_key: str

    __tablename__ = "easypost"

    id = db.Column(db.Integer, primary_key=True)
    key_name = db.Column(db.String(100), nullable=False)
    api_key = db.Column(db.Text, nullable=False)

@dataclass
class Stripe(db.Model):
    id: int
    public_api_key: str
    private_api_key: str

    __tablename__ = "stripe"

    id = db.Column(db.Integer, primary_key=True)
    public_api_key = db.Column(db.Text, nullable=False)
    private_api_key = db.Column(db.Text, nullable=False)


