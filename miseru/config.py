"""
Global Flask Application Setting

See `.flaskenv` for default settings.
 """

import os


class Config(object):
    FLASK_ENV = os.getenv("FLASK_ENV", "production")
    SECRET_KEY = os.getenv("FLASK_SECRET", "Secret")
    JWT_SECRET_KEY = os.getenv("JWT_SECRET_KEY", "Secret")

    FLASK_DEBUG = os.getenv("FLASK_DEBUG")

    APP_DIR = os.path.dirname(__file__)
    ROOT_DIR = os.path.dirname(APP_DIR)
    DIST_DIR = os.path.join(ROOT_DIR, "dist")

    SENTRY_DSN = os.environ.get("SENTRY_DSN")

    REDIS_URL = os.environ.get("REDIS_URL")

    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL")
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    if not os.path.exists(DIST_DIR):
        raise Exception("DIST_DIR not found: {}".format(DIST_DIR))
