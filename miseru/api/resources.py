"""
REST API Resource Routing
http://flask-restplus.readthedocs.io
"""

import uuid
import pickle
from datetime import datetime, timedelta
from functools import wraps
from math import ceil
import html.parser

from validr import T, Compiler, modelclass

import jwt

from flask_jwt_extended import (
    jwt_required,
    create_access_token,
    jwt_refresh_token_required,
    create_refresh_token,
    get_jwt_identity,
)

from flask import request, current_app, jsonify
from flask_restplus import Resource, abort
from flask_cors import cross_origin

from miseru.models import db, User, WooStore
from miseru.config import Config
from .security import require_auth
from . import api_rest
from miseru.extensions import redis_client

from woocommerce import API

import gspread
import stripe

from oauth2client.service_account import ServiceAccountCredentials

FROM_NAME = "Custom Made in PR"
FROM_STREET_1 = "P.O. Box 360812"
FROM_STREET_2 = ""
FROM_CITY = "San Juan"
FROM_STATE = "PR"
FROM_ZIP = "00936"
FROM_COUNTRY = "US"
FROM_PHONE = "787-308-9754"


def get_wcapi(store_id):
    store = WooStore.query.get(store_id)

    return API(url=store.woo_url,
               consumer_key=store.woo_consumer_key,
               consumer_secret=store.woo_consumer_secret,
               timeout=60,
               version="wc/v3")

def get_easypost(store_id):
    import easypost
    store = WooStore.query.get(store_id)
    easypost.api_key = store.easypost.api_key

    return easypost

def authorize(f):
    @wraps(f)
    def decorated_function(*args, **kws):
        email = get_jwt_identity()
        user = User.query.filter(User.email == email).first()

        if not user.authorized:
            abort(403, "Not authorized to carry out this section.")

        return f(*args, **kws)

    return decorated_function

@api_rest.route("/stores/")
class Stores(Resource):
    @jwt_required
    @authorize
    def get(self):
        stores = WooStore.query.all()

        return [{ "id": store.id, "store_name": store.store_name } for store in stores]

@api_rest.route("/store/<int:store_id>/shipping/label/")
class ShippingLabels(Resource):
    @jwt_required
    @authorize
    def post(self, store_id):
        wcapi = get_wcapi(store_id)

        resource_data = request.get_json()
        address_data = resource_data["shipping"]
        billing_data = resource_data["billing"]
        line_items = resource_data["line_items"]
        order_id = resource_data["id"]

        try:
            order = wcapi.get(f"orders/{order_id}").json()
        except e:
            abort(f"Error: %{e}")

        label = list(filter(lambda o: o["key"] == "label_url", order["meta_data"]))

        if label:
            abort(403, "Labels were already created")

        total_tshirt_count = 0
        for item in line_items:
            tshirts = [meta for meta in item["meta_data"] if meta["key"] == "pa_size"]
            if tshirts:
                total_tshirt_count += item["quantity"]

        total_label_count = ceil(total_tshirt_count / 2)

        shipments = [
            create_label(address_data, billing_data, store_id) for i in range(total_label_count)
        ]

        data = create_meta_data(shipments)

        try:
            wcapi.put(f"orders/{order_id}", data).json()
        except e:
            abort(f"Error: %{e}")

        return {"label_urls": get_shipping_label_urls(shipments)}


def create_meta_data(shipments):
    data = {
        "meta_data": [
            {
                "key": "tracking_provider",
                "label": "Tracking Provider",
                "value": get_tracker_data(shipments, "carrier"),
            },
            {
                "key": "tracking_number",
                "label": "Tracking Number",
                "value": get_tracker_data(shipments, "tracking_code"),
            },
            {
                "key": "tracking_link",
                "label": "Tracking Link",
                "value": get_tracker_data(shipments, "public_url"),
            },
            {
                "key": "label_url",
                "label": "Label URL",
                "value": get_shipping_label_urls(shipments),
            },
        ]
    }

    return data


def get_tracker_data(shipments, key):
    result = []
    for shipment in shipments:
        tracker = shipment["tracker"]
        result.append(tracker[key])

    return result


def get_shipping_label_urls(shipments):
    urls = []
    for shipment in shipments:
        urls.append(shipment.postage_label.label_url)

    return urls


def create_label(address_data, billing_data, store_id):
    easypost = get_easypost(store_id)

    to_address = easypost.Address.create(
        verify=["delivery"],
        name=address_data["first_name"] + " " + address_data["last_name"],
        street1=address_data["address_1"],
        street2=address_data["address_2"],
        city=address_data["city"],
        state=address_data["state"],
        zip=address_data["postcode"],
        country=address_data["country"],
        phone=billing_data["phone"],
    )

    from_address = easypost.Address.create(
        verify=["delivery"],
        name=FROM_NAME,
        street1=FROM_STREET_1,
        street2=FROM_STREET_2,
        city=FROM_CITY,
        state=FROM_STATE,
        zip=FROM_ZIP,
        country=FROM_COUNTRY,
        phone=FROM_PHONE,
    )

    try:
        parcel = easypost.Parcel.create(predefined_package="Flat", weight=13)

    except easypost.Error as e:
        if e.param is not None:
            print("Specifically an invalid param: %r" % e.param)
            abort("Specifically an invalid param: %r" % e.param)

    # create shipment
    shipment = easypost.Shipment.create(
        to_address=to_address, from_address=from_address, parcel=parcel
    )

    shipment.buy(rate=shipment.lowest_rate())

    return shipment


@api_rest.route("/store/<int:store_id>/orders/")
@api_rest.route("/store/<int:store_id>/orders/<int:order_id>")
class WooOrders(Resource):
    @jwt_required
    @authorize
    def get(self, store_id):
        wcapi = get_wcapi(store_id)

        status = request.args.get("status")
        page = request.args.get("page")
        orders = wcapi.get(f"orders?status={status}&page={page}&")
        total_pages = orders.headers["X-WP-TotalPages"]
        json = orders.json()

        if len(json) > 0:
            json[0]["total_pages"] = total_pages

        return json

    @jwt_required
    @authorize
    def put(self, store_id, order_id):
        wcapi = get_wcapi(store_id)

        data = request.json["data"]
        return wcapi.put(f"orders/{order_id}", data).json()


@api_rest.route("/store/<int:store_id>/search/products/")
class WooProductSearch(Resource):
    @jwt_required
    @authorize
    def post(self, store_id):
        wcapi = get_wcapi(store_id)

        product_ids = request.json["productIds"]
        product_ids = ",".join(map(str, product_ids))
        products = wcapi.get(f"products?include={product_ids}")
        return products.json()

@api_rest.route("/store/<int:store_id>/categories/")
class WooProducts(Resource):
    def get(self, store_id):
        wcapi = get_wcapi(store_id)
        categories = wcapi.get("products/categories")

        return categories.json()

@api_rest.route("/store/<int:store_id>/products/")
class WooProducts(Resource):
    def get(self, store_id):
        wcapi = get_wcapi(store_id)
        products = wcapi.get(f"products")

        return products.json()

@api_rest.route("/store/<int:store_id>/product/<int:product_id>")
class WooProduct(Resource):
    @jwt_required
    @authorize
    def get(self, store_id, product_id):
        wcapi = get_wcapi(store_id)

        product = wcapi.get(f"products/{product_id}")
        return product.json()


@api_rest.route("/register/")
class Register(Resource):
    def post(self):
        data = request.get_json()
        user = User(**data)
        db.session.add(user)
        db.session.commit()
        return user.to_dict(), 201


@api_rest.route("/login/")
class Login(Resource):
    def post(self):
        email = request.json.get("email", None)
        password = request.json.get("password", None)

        user = User.authenticate(email, password)

        if not user:
            return {"message": "Invalid credentials", "authenticated": False}, 422

        ret = {
            "access_token": create_access_token(identity=email),
            "refresh_token": create_refresh_token(identity=email),
        }

        return ret


@api_rest.route("/refresh/")
class RefreshToken(Resource):
    @jwt_refresh_token_required
    @api_rest.response(201, "Token created")
    def post(self):
        current_user = get_jwt_identity()

        ret = {"access_token": create_access_token(identity=current_user)}

        return ret, 201

@api_rest.route("/store/<int:store_id>/description/")
class StoreDescription(Resource):
    def get(self, store_id):
        rows = get_product_rows("DataTienda")
        row = rows[0]

        return {
            "name": row["nombre"],
            "description": row["descripción"],
            "about": row["sobre-nosotros"],
        }

def get_product_rows(sheet_name):
    scope = ["https://spreadsheets.google.com/feeds", "https://www.googleapis.com/auth/drive"]
    creds = ServiceAccountCredentials.from_json_keyfile_name("miseru/credentials.json", scope)

    client = gspread.authorize(creds)
    sheet = client.open(sheet_name).sheet1

    all_rows = sheet.get_all_records()

    return all_rows[1:]

class HTMLTextExtractor(html.parser.HTMLParser):
    def __init__(self):
        super(HTMLTextExtractor, self).__init__()
        self.result = [ ]

    def handle_data(self, d):
        self.result.append(d)

    def get_text(self):
        return "".join(self.result)

def html_to_text(html):
    """Converts HTML to plain text (stripping tags and converting entities).
    >>> html_to_text('<a href="#">Demo<!--...--> <em>(&not; \u0394&#x03b7;&#956;&#x03CE;)</em></a>')
    'Demo (\xac \u0394\u03b7\u03bc\u03ce)'

    "Plain text" doesn't mean result can safely be used as-is in HTML.
    >>> html_to_text('&lt;script&gt;alert("Hello");&lt;/script&gt;')
    '<script>alert("Hello");</script>'

    Always use html.escape to sanitize text before using in an HTML context!

    HTMLParser will do its best to make sense of invalid HTML.
    >>> html_to_text('x < y &lt z <!--b')
    'x < y < z '

    Unrecognized named entities are included as-is. '&apos;' is recognized,
    despite being XML only.
    >>> html_to_text('&nosuchentity; &apos; ')
    "&nosuchentity; ' "
    """
    s = HTMLTextExtractor()
    s.feed(html)
    return s.get_text()

def prepare_stripe_line_items(products):
    entries_to_remove = ("id", "price", "slug", "attributes", "chosenAtts", "dispId")

    for p in products:
        new_images = [i["src"] for i in p["images"]]
        p["images"] = new_images
        p["description"] = html_to_text(p["description"])

        for k in entries_to_remove:
            p.pop(k, None)

    return products

def prepare_woo_line_items(products):
    line_items = []

    for p in products:
        li = {
            "product_id": p["id"],
            "quantity": p["quantity"]
        }

        if "chosenAtts" in p:
            var = {}
            for a in p["chosenAtts"]:
                slug = "attribute_pa_" + a["name"].lower()
                var[slug] = a["option"]

            li["variations"] = var

        line_items.append(li)

    return line_items

@modelclass
class Contact:
    phone=T.str.desc("Contact phone")
    email=T.email.desc("Contact email")

@modelclass
class Address:
    name=T.str.desc("Name")
    lastName=T.str.desc("Last name")
    country=T.str.desc("Country")
    state=T.str.optional.desc("State")
    line1=T.str.desc("Address line 1")
    line2=T.str.desc("Address line 2")
    city=T.str.desc("City")
    zipcode=T.int.desc("Zipcode")

@modelclass
class Product:
    id=T.int.desc("Woo Product ID")
    chosenAtts=T.list.desc("Chosen attributes")
    quantity=T.int.desc("Item quantity")
    name=T.str.desc("Product name")
    description=T.str.desc("Product description")
    amount=T.int.desc("Amount")
    price=T.float.desc("Price")
    currency=T.str.desc("Currency")
    slug=T.str.desc("Slug")
    images=T.list.desc("Images")

def validate(data, ModelClass):
    compiler = Compiler()
    f = compiler.compile(ModelClass)

    return f(data)

def validate_checkout_request(resource):
    contact = validate(resource["contact"], Contact)
    shipping = validate(resource["shipping"], Address)
    billing = validate(resource["billing"], Address)

    products = [validate(p, Product) for p in resource["products"]]

    return contact, shipping, billing, products

def payment_intent(mongo_id, shipping, contact):
    return {
        "capture_method": "manual",
        "metadata": {
            "mongo_id": mongo_id
        },
        "shipping": {
            "name": shipping["name"] + " " + shipping["lastName"],
            "address": {
                "line1": shipping["line1"],
                "line2": shipping["line2"],
                "city": shipping["city"],
                "state": shipping.get("state"),
                "postal_code": shipping["zipcode"],
                "country": shipping["country"],
            },
            "phone": contact["phone"],
        }
    }

@api_rest.route("/store/<int:store_id>/checkout/")
class StripeCheckout(Resource):
    @cross_origin()
    def post(self, store_id):
        store = WooStore.query.get(store_id)
        stripe.api_key = store.stripe.private_api_key

        resource = request.get_json()

        try:
            contact, shipping, billing, products = validate_checkout_request(resource)
        except:
            return "Bad form", 400

        mongo_id = str(uuid.uuid1())

        session = stripe.checkout.Session.create(
            customer_email = contact["email"],
            payment_method_types=["card"],
            line_items=prepare_stripe_line_items(products),
            success_url=f"{store.store_url}/success",
            cancel_url=f"{store.store_url}/cancel",
            payment_intent_data=payment_intent(mongo_id, shipping, contact)
        )

        redis_client.set(mongo_id, pickle.dumps(resource))

        return session

def generate_order(store_id, contact, shipping, billing, products):
    data = {
        "payment_method": "stripe",
        "payment_method_title": "Stripe",
        "set_paid": True,
        "billing": {
            "first_name": billing["name"],
            "last_name": billing["lastName"],
            "address_1": billing["line1"],
            "address_2": billing["line2"],
            "city": billing["city"],
            "state": billing["state"],
            "postcode": billing["zipcode"],
            "country": billing["country"],
            "email": contact["email"],
            "phone": contact["phone"],
        },
        "shipping": {
            "first_name": shipping["name"],
            "last_name": shipping["lastName"],
            "address_1": shipping["line1"],
            "address_2": shipping["line2"],
            "city": shipping["city"],
            "state": shipping["state"],
            "postcode": shipping["zipcode"],
            "country": shipping["country"],
        },
        "line_items": prepare_woo_line_items(products),
    }

    wcapi = get_wcapi(store_id)

    return wcapi.post("orders", data).json()

@api_rest.route("/store/<int:store_id>/hooks/")
class StripeWebhooks(Resource):
    @cross_origin()
    def post(self, store_id):
        payload = request.data.decode("utf-8")

        received_sig = request.headers.get("Stripe-Signature", None)
        webhook_secret = "whsec_Xu0PTNc1rrElAmTgmhWlIMJpCtMsZFME"

        try:
            event = stripe.Webhook.construct_event(
                payload, received_sig, webhook_secret
            )
        except ValueError:
            print("Error while decoding event!")
            return "Bad payload", 400
        except stripe.error.SignatureVerificationError:
            print("Invalid signature!")
            return "Bad signature", 400

        if event["type"] == "charge.succeeded":
            session = event["data"]["object"]
            p_data = redis_client.get(event["data"]["object"]["metadata"]["mongo_id"])
            data = pickle.loads(p_data)
            generate_order(store_id, data["contact"], data["shipping"], data["billing"], data["products"])

        print(
            "Received event: id={id}, type={type}".format(
                id=event.id, type=event.type
            )
        )

        return "", 200
