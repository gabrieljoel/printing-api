import os

from flask import Flask
from flask_migrate import Migrate

import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration


from .commands import import_products
from .api import api_bp
from .client import client_bp
from .config import Config
from .extensions import db, jwt, sslify, cors, redis_client, wdb


def create_app(app_name="MISERU"):
    app = Flask(app_name, static_folder="../dist/static")

    app.logger.info(">>> {}".format(Config.FLASK_ENV))
    app.config.from_object("miseru.config.Config")

    app.register_blueprint(api_bp)
    app.register_blueprint(client_bp)

    db.init_app(app)
    redis_client.init_app(app)
    jwt(app)
    sslify(app)
    cors(app)

    app.wsgi_app = wdb(app.wsgi_app)

    migrate = Migrate(app, db)

    register_commands(app)

    sentry_sdk.init(dsn=Config.SENTRY_DSN, integrations=[FlaskIntegration()])

    return app

def register_commands(app):
    """Register Click commands."""
    app.cli.add_command(import_products)
