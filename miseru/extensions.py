# -*- coding: utf-8 -*-
"""Extensions module. Each extension is initialized in the app factory located in app.py."""

from flask_sqlalchemy import SQLAlchemy
from flask_redis import FlaskRedis
from flask_jwt_extended import JWTManager
from flask_sslify import SSLify
from flask_cors import CORS
from wdb.ext import WdbMiddleware

db = SQLAlchemy()
jwt = JWTManager
sslify = SSLify
cors = CORS
redis_client = FlaskRedis()
wdb = WdbMiddleware
