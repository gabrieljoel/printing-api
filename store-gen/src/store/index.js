import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

import cart from './modules/cart'
import checkout from './modules/checkout'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

const plugins = []

if (process.browser) {
  const vuexLocal = new VuexPersistence({
    storage: window.localStorage,
    modules: ['cart']
  })

  const vuexSession = new VuexPersistence({
    storage: window.sessionStorage,
    modules: ['checkout']
  })

  plugins.push(vuexLocal.plugin)
  plugins.push(vuexSession.plugin)
}

export default new Vuex.Store({
  modules: {
    cart,
    checkout,
  },
  strict: debug,
  plugins: plugins
})
