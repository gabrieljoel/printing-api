import { getField, updateField } from 'vuex-map-fields'

const state = {
  shipping: {
    name: '',
    lastName: '',
    country: '',
    state: '',
    line1: '',
    line2: '',
    city: '',
    zipcode: '',
  },
  billing: {
    name: '',
    lastName: '',
    country: '',
    state: '',
    line1: '',
    line2: '',
    city: '',
    zipcode: '',
  },
  contact: {
    phone: '',
    email: '',
  },
  differentShipping: false,
  shippingFormValid: false,
  orderFormValid: false,
  contactFormValid: false,
}

// getters
const getters = {
  getField,
}

// mutations
const mutations = {
  updateField,
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
}
