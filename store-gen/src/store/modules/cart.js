import Vue from 'vue'
import isEqual from 'lodash.isequal'

import shop from '../../api/shop'

function round (num) {
  return Math.round(num * 100) / 100
}

// initial state
// shape: [{ id, quantity }]
const state = {
  items: [],
  checkoutStatus: null
}

// getters
const getters = {
  cartProducts: (state) => {
    return state.items
  },

  cartSubtotal: (state, getters) => {
    return getters.cartProducts.reduce((total, product) => {
      return total + product.price * product.quantity
    }, 0)
  },

  cartTotalItems: (state) => {
    var total = 0

    state.items.forEach(function (item) {
      total += item.quantity
    })

    return total
  },

  cartTax: (state, getters) => {
    const num = getters.cartSubtotal * 0.115
    return round(num)
  },

  cartTotal: (state, getters) => {
    return round(getters.cartTax + getters.cartSubtotal)
  }

}

// actions
const actions = {
  checkout ({ commit, state }, products) {
    const savedCartItems = [...state.items]
    commit('setCheckoutStatus', null)
    // empty cart
    commit('setCartItems', { items: [] })
    shop.buyProducts(
      products,
      () => commit('setCheckoutStatus', 'successful'),
      () => {
        commit('setCheckoutStatus', 'failed')
        // rollback to the cart saved before sending the request
        commit('setCartItems', { items: savedCartItems })
      }
    )
  },

  clearCart ({ commit }) {
    commit('setCartItems', { items: [] })
  },

  addProductToCart ({ commit, state }, product) {
    commit('setCheckoutStatus', null)

    const cartItem = state.items.find(item => item.dispId === product.dispId)

    if (!cartItem) {
      commit('pushProductToCart', { product: product })
    } else {

      if (product.hasOwnProperty('chosenAtts')) {

        const compare = (a, b) => {
          return a.id - b.id
        }

        var productAtts = product.chosenAtts
        var itemAtts = cartItem.chosenAtts

        productAtts.sort(compare)
        itemAtts.sort(compare)

        var allEqual = []

        productAtts.forEach((p, i) => {
          allEqual.push(isEqual(p, itemAtts[i]))
        })

        if (allEqual.every(e => e === true)) {
          commit('incrementItemQuantity', cartItem)
        } else {
          commit('pushProductToCart', { product: product })
        }
      }
      else {
        commit('incrementItemQuantity', cartItem)
      }
    }
  },

  removeProductFromCart ({ commit, state }, product) {
    const filtered = state.items.filter(function (item, index, arr) {
      return item.dispId !== product.dispId
    })

    commit('setCartItems', { items: filtered })
  },

  setItemQuantity ({ state, commit }, cartObj) {
    const product = cartObj.product
    const quantity = cartObj.$event

    commit('setItemQuantity', { id: product.dispId, quantity: quantity })
  }
}

// mutations
const mutations = {
  pushProductToCart (state, { product }) {
    Vue.set(product, 'quantity', 1)
    state.items.push(product)
  },

  incrementItemQuantity (state, { id }) {
    const cartItem = state.items.find(item => item.id === id)
    cartItem.quantity++
  },

  setItemQuantity (state, { id, quantity }) {
    const cartItem = state.items.find(item => item.dispId === id)
    cartItem.quantity = quantity
  },

  setCartItems (state, { items }) {
    state.items = items
  },

  setCheckoutStatus (state, status) {
    state.checkoutStatus = status
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
