// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import '../node_modules/@mdi/font/scss/materialdesignicons.scss'

import DefaultLayout from '~/layouts/Default.vue'
import store from './store'

import Buefy from 'buefy'
import 'buefy/dist/buefy.css'


export default function (Vue, { router, head, isClient, appOptions }) {
  // Set default layout as a global component
  appOptions.store = store

  Vue.use(Buefy)

  Vue.component('Layout', DefaultLayout)
}
