export default {
  methods: {
    validity () {
      this.$emit('valid', this.$refs.form.checkValidity(), this.formName)
    }
  },
  watch: {
    form: {
      handler: function (val, oldVal) {
        this.validity()
      },
    }
  },
  mounted: function () {
    this.validity()
  }
}
