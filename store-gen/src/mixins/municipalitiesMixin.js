import municipalities from '../../static/pr-municipalities.json'

export default {
  computed: {
    municipalities () {
      return municipalities
    },
  }
}
