import states from '../../static/states-hash.json'

export default {
  computed: {
    states () {
      return states
    },
  }
}
