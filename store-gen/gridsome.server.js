// Server API makes it possible to hook into various parts of Gridsome
// on server-side and add custom data to the GraphQL data layer.
// Learn more: https://gridsome.org/docs/server-api/

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

const axios = require('axios')

module.exports = function (api) {
  const storeId = process.env.GRIDSOME_STORE_ID

  api.createPages(({ createPage }) => {
    // Use the Pages API here: https://gridsome.org/docs/pages-api/
  })

  api.loadSource(async actions => {
    const { data: categoryData } = await axios.get(`http://127.0.0.1:5000/api/store/${storeId}/categories/`)
    const { data: productData } = await axios.get(`http://127.0.0.1:5000/api/store/${storeId}/products/`)

    const categories = actions.addCollection({
      typeName: 'Category'
    })

    const products = actions.addCollection({
      typeName: 'Product'
    })

    products.addReference('categories', 'Category')

    for (const item of categoryData) {
      if (item.name !== 'Uncategorized') {
        categories.addNode({
          id: item.id,
          name: item.name,
          description: item.description,
          slug: item.slug,
          image: item.image
        })
      }
    }

    for (const item of productData) {
      let categoryIds = item.categories.map(c => c.id)

      products.addNode({
        id: item.id,
        name: item.name,
        slug: item.slug,
        price: item.price,
        amount: item.price * 100,
        images: item.images,
        attributes: item.attributes,
        description: item.description,
        categories: categoryIds,
        currency : 'usd'
      })
    }
  })

  api.loadSource(async actions => {
    const { data } = await axios.get(`http://127.0.0.1:5000/api/store/${storeId}/description/`)
    const collection = actions.addCollection('Store')

    collection.addNode({
      id: 1,
      name: data.name,
      description: data.description,
      about: data.about,
    })
  })
}
